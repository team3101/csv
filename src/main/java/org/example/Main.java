package org.example;

import org.example.csv.Csv;
import org.example.csv.CsvLib;

public class Main {
  public static void main(String[] args) {
    System.out.println("Without API");
    Csv reader = new Csv("users.csv");
    reader.showData();
    System.out.println("-------------");
    System.out.println("With API");
    CsvLib csvReader = new CsvLib("users.csv");
    csvReader.showData();
  }
}