package org.example.csv;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvLib {
  CSVReader reader;
  List<List<String>> data;

  public CsvLib(String file) {
    data = new ArrayList<>();
    try {
      reader = new CSVReaderBuilder(
              new FileReader("src/main/resources/data/" + file)).withSkipLines(1).build();
      /*
          ParserBuilder allows you to choose a custom column separator,
          ore or handle quotations marks, decide what to do with null fields,
          how to interpret escaped characters:
          Parser parser = new CSVParserBuilder()
            .withSeparator('\t')
            .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_QUOTES)
            .withIgnoreLeadingWhiteSpace(true)
            .withIgnoreQuotations(false)
            .withStrictQuotes(true)
            .build();
          CSVReader csvReader = new CSVReaderBuilder(reader)
             .withSkipLines(1)
             .withCSVParser(parser)
             .build();
       */
      getData();
    } catch (CsvValidationException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void getData() throws CsvValidationException, IOException {
    String[] values;
    while ((values = reader.readNext()) != null) {
      data.add(Arrays.asList(values));
    }
  }

  public void showData() {
    data.forEach(System.out::println);
  }
}
