package org.example.csv;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * AddJavaDoc.
 */
public class Csv {

  List<List<String>> data;
  BufferedReader br;

  /**
   * AddJavaDoc.
   *
   * @param file Add.
   */
  public Csv(String file) {
    this.data = new ArrayList<>();
    try {
      this.br = new BufferedReader(new FileReader("src/main/resources/data/" + file));
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
    getData();
  }

  /**s UP-TO-DATE
   > Task :app:jar UP-TO-DATE

   > Task :app:Main.main() FAILED
   Error occurred duri
   * AddJavaDoc.
   */
  public void getData() {
    try {
      String line;
      br.readLine();
      while ((line = br.readLine()) != null) {
        String[] values = line.split(",");
        data.add(Arrays.asList(values));
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * AddJavaDoc.
   */
  public void showData() {
    data.forEach(System.out::println);
  }
}
